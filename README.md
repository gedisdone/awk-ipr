# README #

This AWK script looks for monochrome patterns in an image defined as 'source' in CSV file.
CSV file format:

| Pattern name | File name |
| --- | --- |
| source | path to the image where the script seeks for patterns |
| bgcolor | background color to ignore in patterns and the source image |
| any_other_name | path to the pattern the script looks for in the image |

All images follow pretty strict subset of PPM format: 'P3' goes in the first line,
'width height' in the second one, '255' in the third line and the rest of lines for the image.
`convert input.png -compress none output.ppm` should do the job.

The output as follows:

| Pattern name | RGB color | Pixel row | Pixel column |

Example of the source image `source_test.ppm` with patterns to recognise, 
input file `example.csv` and output file `patterns.csv` you may find in the `example` directory.

Top-left pixel is considered to be (1,1).

It is possible to perform the reverse job with the output provided, maybe it's yet to be written.