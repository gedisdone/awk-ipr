#!/usr/bin/awk -f

function read_ppm(name, fname) {
    _read_ppm_line = 0;
    _read_ppm_w = 1;
    _read_ppm_h = 1;
    _read_ppm_subp = 2;
    while (getline < fname) {
	_read_ppm_line++;
	if ($0 ~ /^\#/) { _read_ppm_line--; continue; }

	if (_read_ppm_line == 1) {
	    if ($0 != "P3") {break;}
	}
	if (_read_ppm_line == 2) {
	    files[name,"width"] = $1;
	    files[name,"height"] = $2;
	}
	if (_read_ppm_line == 3) {
	    if ($1 != "255") { break; }
	}

	if (_read_ppm_line <= 3) { continue; }

	for (i = 1; i <= NF; i++) {
	    files[name,_read_ppm_w,_read_ppm_h] += lshift($i,(8 * _read_ppm_subp))
	    _read_ppm_subp--;
	    if (_read_ppm_subp == -1) {
		_read_ppm_subp = 2;
		_read_ppm_w++;
		if (_read_ppm_w > files[name,"width"]) {
		    _read_ppm_w = 1;
		    _read_ppm_h++;
		}
	     }
	}
    }
}

function seek_patterns() {
    for (pix_h = 1; pix_h <= files["source","height"]; pix_h++) {
	for (pix_w = 1; pix_w <= files["source","width"]; pix_w++) {
	    for (i in props) {
		if (i == "source") { continue; }
		id_status = identify_pat(i, pix_w, pix_h);
		if (id_status >= 0) {
		    print i, id_status, pix_w, pix_h; # Results of pattern detection
		}
	    }
	}
    }
}

function identify_pat(_ip_name, _ip_w, _ip_h) {
    broken = 0;
    _ip_patcolor = bgcolor;
    for (cur_h = 1; cur_h <= files[_ip_name,"height"]; cur_h++) {
	for (cur_w = 1; cur_w <= files[_ip_name,"width"]; cur_w++) {
	    src_w = _ip_w + cur_w - 1;
	    src_h = _ip_h + cur_h - 1;

	    if (src_w > files["source","width"] || src_h > files["source","height"]) {
		broken = -1; break; # As you can't detect a pattern out of bounds of the source image
	    }

	    cur_s_color = files["source", src_w, src_h];
	    cur_n_color = files[_ip_name, cur_w, cur_h];

	    if (   cur_s_color != bgcolor \
		&& _ip_patcolor == bgcolor ) {
		_ip_patcolor = cur_s_color; # This condition is excepted to be executed once,
                                            # when we find a pixel colored not bgcolor
	    }
	    if (   cur_s_color != _ip_patcolor \
		&& cur_s_color != bgcolor ) {
		broken = -1; break; # We detect only monochrome patterns
	    }
	    if ( !((   cur_s_color == _ip_patcolor \
		    && _ip_patcolor != bgcolor \
		    && cur_n_color != bgcolor) \
		|| (   cur_s_color == bgcolor \
		    && cur_n_color == bgcolor))) { # Either pixels have non-bgcolor, or both are colored bgcolor
		broken = -1; break;

	    }
	}

	if (broken == -1) { break; }
    }

    if (broken != 0) {
	return broken;
    } else {
	return _ip_patcolor;
    }
}

$1 !~ /bgcolor/ {
    props[$1] = $2;
}

$1 ~ /bgcolor/ {
    bgcolor = $2;
}

END {
    for (x in props) {
	read_ppm(x, props[x]);
    }
    seek_patterns();
}